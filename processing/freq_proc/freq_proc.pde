import processing.sound.*;
import processing.serial.*; 

Serial port;

FFT fft;
AudioIn in;
int bands = 8192;
int max_freq = 0, real_max = 0;
int count = 0;
float[] spectrum = new float[bands];


int sheet[] = {233, 262, 294, 311, 349, 392, 440, 466, 0, 466,440, 392, 349, 311, 294, 262, 233, 0, 1000};
//int sheet[] = {233, 262, 294 ,233,294 ,233,294, 0, 262, 294,311,294,262,311,0,294,311,349,294,349,294,349,0,1000};

void setup() {
  size(4096, 360);
  background(255);
  
  port = new Serial(this, "COM10", 9600);
  
  fft = new FFT(this, bands);
  in = new AudioIn(this, 0);
  
  // start the Audio Input
  in.start();  
  fft.input(in);
}      

void draw() { 
  background(255);
  fft.analyze(spectrum);
  for(int i=1; i< bands; i++){
    if(spectrum[i] > 0.01){
      max_freq = i;
      break;
    }
  }
  real_max = max_freq;
  for(int j = max_freq; j<max_freq*1.5; j++){
    if(spectrum[real_max] < spectrum[j]){
      real_max = j;
    }
  }
  //println("receiving");
  for(int i = 0; i < bands; i++){ 
    /*
       2.683(=220/82) deducted from practical measurment
       82   at 220 Hz
       163  at 440 Hz
       328  at 880 Hz
    */
    line( i, height, i, height - spectrum[i]*height*5 );
  }
  if(spectrum[real_max] > 0.03){
    println("Frequency : ", real_max*2.683);
    if(real_max*2.683-sheet[count] < 5 && real_max*2.683-sheet[count] > -5){
      count ++;
    }
  }
  if(sheet[count] == 0){
    println("##########################################PAGE TURN");
    port.write(1);
    count++;
    port.write(0);
  }
}
/*
void signal() {
  if(max_freq*2.683 > 1000) {
    println("moving");
    port.write(1);
  }
  else{
    port.write(0);
    println("not moving");  
  }
}
*/
